package net.welovespigotplugins.teamchat.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.welovespigotplugins.teamchat.TeamChat;
import net.welovespigotplugins.teamchat.objects.PlayerInfo;

/**
 * JavaDoc this file!
 * Created: 30.07.2018
 *
 * @author WeLoveSpigotPlugins (welovespigotplugins@gmail.com)
 */
public class TeamChatCommand extends Command {
    public TeamChatCommand(String name) {
        super(name);
    }

    public void execute(CommandSender commandSender, String[] args) {

        if (!(commandSender instanceof ProxiedPlayer))
            return;

        final ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;

        if (!proxiedPlayer.hasPermission("teamchat.use")) {
            proxiedPlayer.sendMessage(TeamChat.PREFIX + "§7Du hast keine §cRechte§7, um dies zu tun§8!");
            return;
        }

        if(args.length == 0) {

            proxiedPlayer.sendMessage(TeamChat.PREFIX + "§7Online Members:");

            for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                final PlayerInfo playerInfo = TeamChat.getInstance().getPlayerInfo(all.getUniqueId());
                proxiedPlayer.sendMessage("§8» §a" + all.getName() +
                        "§8 (§a" + all.getServer().getInfo().getName() + "§8) §8» (" +
                        (playerInfo.isLoggedIn() ? "§aEingeloggt" : "§cAusgeloggt" + "§8)"));
            }

            return;
        }

        if (args.length == 1 &&
                (args[0].equalsIgnoreCase("login") || args[0].equalsIgnoreCase("logout"))) {

            switch (args[0].toLowerCase()) {

                case "login":

                    if(TeamChat.getInstance().getPlayerInfo(proxiedPlayer.getUniqueId()).isLoggedIn()) {
                        proxiedPlayer.sendMessage(TeamChat.PREFIX + "§7Du bist bereits eingeloggt§8!");
                        return;
                    }

                    for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                        if(all.hasPermission("teamchat.use")) {
                            if(TeamChat.getInstance().getPlayerInfo(all.getUniqueId()).isLoggedIn()) {
                                all.sendMessage(TeamChat.PREFIX + "§a" + proxiedPlayer.getName() + "§7 hat sich in den Teamchat eingeloggt§8!");
                            }
                        }
                    }

                    TeamChat.getInstance().getPlayerInfo(proxiedPlayer.getUniqueId()).setLoggedIn(true);
                    proxiedPlayer.sendMessage(TeamChat.PREFIX + "§7Du hast dich eingeloggt§8!");

                    break;

                case "logout":

                    if(!TeamChat.getInstance().getPlayerInfo(proxiedPlayer.getUniqueId()).isLoggedIn()) {
                        proxiedPlayer.sendMessage(TeamChat.PREFIX + "§7Du bist bereits ausgeloggt§8!");
                        return;
                    }

                    for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                        if(all.hasPermission("teamchat.use")) {
                            if(TeamChat.getInstance().getPlayerInfo(all.getUniqueId()).isLoggedIn()) {
                                all.sendMessage(TeamChat.PREFIX + "§a" + proxiedPlayer.getName() + "§7 hat sich vom Teamchat entfernt§8!");
                            }
                        }
                    }

                    TeamChat.getInstance().getPlayerInfo(proxiedPlayer.getUniqueId()).setLoggedIn(false);
                    proxiedPlayer.sendMessage(TeamChat.PREFIX + "§7Du hast dich ausgeloggt§8!");

                    break;

            }

        }

        final StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < args.length ; i++) {
            stringBuilder.append(args[i] + " ");
        }

        for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if(all.hasPermission("teamchat.use") &&
                    TeamChat.getInstance().getPlayerInfo(all.getUniqueId()).isLoggedIn()) {
                all.sendMessage(TeamChat.PREFIX + "§8 » §a" + proxiedPlayer.getName() +
                        "§8 » §f" + stringBuilder.toString());
            }
        }

    }

}
